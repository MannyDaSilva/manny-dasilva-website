# About Me
Hi, I'm Manny! I'm a full stack software engineer based in Rhode Island, and I currently work at Rite Solutions Inc. in Middletown, RI, while also
taking on freelance projects, such as this one! I have a strong passion for software development and am always looking to broaden my horizons by improving
my skillset, which is one of the key reasons behind this project.

# Project Details
This is my personal website project that includes information about me that ranges from my software development experience to bodybuilding journey.
This project was built using Javascript, React, HTML, CSS, and Node.js.
Below is the list of content this website offers:
◦ Home Page
◦ About Me Page
◦ Software Development Page
◦ Bodybuilding Page
◦ My Socials
◦ Contact Information 

# Personal Links
GitLab: https://gitlab.com/MannyDaSilva
LinkedIn: https://www.linkedin.com/in/manny-dasilva-jr-3a01ba1ab/

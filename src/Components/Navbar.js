import React, {useState} from 'react'
import { Link } from 'react-router-dom';
import './Navbar.css';

function Navbar() {
    const [click, setClick] = useState(false);

    const handleClick = () => setClick(!click);

    const closeMenu = () => setClick(false);

    return (
        <>
            <nav className='navbar'>
                <div className='navbar-container'>
                    <Link to='/' className='navbar-logo'>
                        <img src="/public/Images/logo.png" alt="manny-logo"/>
                    </Link>
                    <div className='menu-icon' onClick={handleClick}>
                        <i className={click ? 'fas fa-times' : 'fas fa-bars'}/>
                    </div>
                    <ul className={click ? 'nav-menu active' : 'nav-menu'}>
                        <li className='nav-item'>
                            <Link to='/home' className='nav-links' onClick={closeMenu}>
                                Home
                            </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/about-me' className='nav-links' onClick={closeMenu}>
                                About Me
                            </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/software-development' className='nav-links' onClick={closeMenu}>
                                Software Development
                            </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/bodybuilding' className='nav-links' onClick={closeMenu}>
                                Bodybuilding
                            </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/socials' className='nav-links' onClick={closeMenu}>
                                Socials
                            </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/contact-me' className='nav-links' onClick={closeMenu}>
                                Contact Me
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    )
}

export default Navbar
